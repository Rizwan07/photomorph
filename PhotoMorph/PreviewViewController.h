//
//  PreviewViewController.h
//  PhotoMorph
//
//  Created by Rizwan on 5/17/13.
//  Copyright (c) 2013 com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PreviewViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIImageView *imageViewPreview;
@property (strong, nonatomic) UIImage *image;

- (IBAction)doneBtnPressed:(id)sender;
@end
