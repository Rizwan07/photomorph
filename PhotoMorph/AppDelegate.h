//
//  AppDelegate.h
//  PhotoMorph
//
//  Created by Rizwan on 5/8/13.
//  Copyright (c) 2013 com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (assign, nonatomic) BOOL isPickerController;

@end
