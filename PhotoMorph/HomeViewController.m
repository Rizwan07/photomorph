//
//  ViewController.m
//  PhotoMorph
//
//  Created by Rizwan on 5/8/13.
//  Copyright (c) 2013 com. All rights reserved.
//

#import "HomeViewController.h"
#import "DetailViewController.h"
#import "TWSReleaseNotesView.h"
#import "RageIAPHelper.h"

#define degreesToRadians(x) (M_PI * (x) / 180.0)
#define kAnimationRotateDeg 1.0

@interface HomeViewController () {
    
    NSArray *_products;
    UITapGestureRecognizer *_tapGesture;
    int currentImageIndex;
    int categoryIndex;
    IBOutlet UIButton *btnBlackWhite;
    IBOutlet UIButton *btnColored;
    
//    IBOutlet UIButton *imageBtn;
    IBOutlet UIButton *okBtn;

    NSArray *arrPics;
    
    NSArray *arrGreyPics;
    NSArray *arrColorPics;
    
    NSMutableArray *arrPurchasedImages;
    NSMutableArray *arrCollection;
    int count;
    NSTimer *timer;
    IBOutlet UIView *viewForCollection;
    
    IBOutlet UIActivityIndicatorView *activityIndicator;
    IBOutlet UICollectionView *myCollectionView;
    IBOutlet UILabel *lblPurchasedAllItems;
}

- (IBAction)cancelBtnPressed:(id)sender;
- (IBAction)okBtnPressed:(id)sender;

- (IBAction)blackNWhiteBtnPressed:(id)sender;
- (IBAction)coloredBtnPressed:(id)sender;

@end

@implementation HomeViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    [viewForCollection.layer setBorderWidth:5.0];
    [viewForCollection.layer setCornerRadius:5.0];
    [viewForCollection.layer setMasksToBounds:YES];

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];

    if (![defaults boolForKey:@"notDownloadedFirstTime"]) {
        [defaults setBool:YES forKey:@"notDownloadedFirstTime"];
        
        NSArray *arr1 = [NSArray arrayWithObjects:@"grey-01", @"grey-02", nil];
        NSArray *arr2 = [NSArray arrayWithObjects:@"color-01", @"color-02", @"color-03", nil];
        [defaults setObject:[arr1 componentsJoinedByString:@"_"] forKey:@"black"];
        [defaults setObject:[arr2 componentsJoinedByString:@"_"] forKey:@"color"];
    }
    
    NSString *strGreyPics = [defaults objectForKey:@"black"];
    
    arrGreyPics = [[defaults objectForKey:@"black"] componentsSeparatedByString:@"_"];
    arrColorPics = [[defaults objectForKey:@"color"] componentsSeparatedByString:@"_"];
    
    
//    NSArray *arrGreyPics = [strGreyPics componentsSeparatedByString:@"_"];
//    for (int i = [arrGreyPics count] - 1; i < 19; i ++) {
//        strGreyPics = [strGreyPics stringByAppendingFormat:@"_%@", @"lock-cover"];
//    }
    for (int i = 1; i <= 19; i ++) {
        NSString *strName = [NSString stringWithFormat:@"grey-%02d", i];
        if ([strGreyPics rangeOfString:strName].location == NSNotFound) {
            
            strGreyPics = [strGreyPics stringByAppendingFormat:@"_%@", strName];
        }
    }
    
    arrPics = [strGreyPics componentsSeparatedByString:@"_"];
    
    categoryIndex = 0;
    count = 0;
//    arrPics = [NSArray arrayWithObjects:@"grey-01.png", @"grey-02.png", @"grey-03.png", @"grey-04.png", @"grey-05.png", @"grey-06.png", @"grey-07.png", @"grey-08.png", nil];

    arrCollection = [NSMutableArray arrayWithObjects:@"color-01", @"color-02", @"color-03", @"color-04", @"color-05", @"color-06", @"color-07", @"color-08", @"color-09", @"color-10", @"color-11", @"color-12", @"color-13", @"color-14", @"color-15", @"color-16", @"color-17", @"color-18", @"color-19", @"color-20", @"color-21", @"grey-01", @"grey-02", @"grey-03", @"grey-04", @"grey-05", @"grey-06", @"grey-07", @"grey-08", @"grey-09", @"grey-10", @"grey-11", @"grey-12", @"grey-13", @"grey-14", @"grey-15", @"grey-16", @"grey-17", @"grey-18", @"grey-19", nil];
    
    arrPurchasedImages = [NSMutableArray array];
    
    [self reload];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    viewiCarousel.type = iCarouselTypeCoverFlow;
    [viewiCarousel reloadData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productPurchased:) name:IAPHelperProductPurchasedNotification object:nil];
}

- (IBAction)doneBtnPressed:(id)sender {
    
    if ([viewiCarousel isScrolling]) {
        return;
    }
    
    UIImageView *imgV = (UIImageView *)[viewiCarousel currentItemView];
    
    if (![imgV.subviews[0] isHidden] && ([arrColorPics containsObject:arrPics[currentImageIndex]] || [arrGreyPics containsObject:arrPics[currentImageIndex]])) {
    
        DetailViewController *detailViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailViewController"];
        [detailViewController setStrUpperImage:arrPics[currentImageIndex]];
        [detailViewController setCategoryIndex:categoryIndex];
        [self.navigationController pushViewController:detailViewController animated:YES];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please select valid Image first" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (IBAction)arrowBackBtnPressed:(id)sender {
    
    [viewiCarousel scrollToItemAtIndex:viewiCarousel.currentItemIndex-1 animated:YES];
}

- (IBAction)arrowForwardBtnPressed:(id)sender {

    [viewiCarousel scrollToItemAtIndex:viewiCarousel.currentItemIndex+1 animated:YES];
}

- (void)tapView:(UITapGestureRecognizer *)gesture {
    
    [self.view setUserInteractionEnabled:NO];

    [UIView transitionWithView:viewForTable duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        [viewForTable setHidden:YES];
    } completion:^(BOOL finished) {
        
        [self.view setUserInteractionEnabled:YES];
    }];
    
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    
    if ([touch.view isKindOfClass:[UIControl class]]) {
        return NO;
    }
    return YES;
}

- (IBAction)categoryBtnPressed:(id)sender {
    
    [self.view bringSubviewToFront:viewForTable];
    
    if ([viewForTable isHidden]) {
        
        [self.view setUserInteractionEnabled:NO];
        
        [UIView transitionWithView:viewForTable duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
            [viewForTable setHidden:NO];
        } completion:^(BOOL finished) {
            
            [self.view setUserInteractionEnabled:YES];
        }];
    }
    else {
        [self tapView:nil];
    }

}

- (IBAction)blackNWhiteBtnPressed:(id)sender {
    
    [btnBlackWhite setSelected:YES];
    [btnColored setSelected:NO];
    
    [self categoryBtnPressed:nil];
    
    categoryIndex = 0;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *strGreyPics = [defaults objectForKey:@"black"];
    
    for (int i = 1; i <= 19; i ++) {
        NSString *strName = [NSString stringWithFormat:@"grey-%02d", i];
        if ([strGreyPics rangeOfString:strName].location == NSNotFound) {
            
            strGreyPics = [strGreyPics stringByAppendingFormat:@"_%@", strName];
        }
    }
    
    
//    NSArray *arrGreyPics = [strGreyPics componentsSeparatedByString:@"_"];
//    for (int i = [arrGreyPics count] - 1; i < 19; i ++) {
//        strGreyPics = [strGreyPics stringByAppendingFormat:@"_%@", @"lock-cover"];
//    }
    
    arrPics = [strGreyPics componentsSeparatedByString:@"_"];
    
    //arrPics = [NSArray arrayWithObjects:@"grey-01.png", @"grey-02.png", @"grey-03.png", @"grey-04.png", @"grey-05.png", @"grey-06.png", @"grey-07.png", @"grey-08.png", nil];
    
    [viewiCarousel reloadData];
    [viewForTable setHidden:YES];
    
}

- (IBAction)coloredBtnPressed:(id)sender {
    
    [btnBlackWhite setSelected:NO];
    [btnColored setSelected:YES];
    
    [self categoryBtnPressed:nil];
    
    categoryIndex = 1;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSString *strColorPics = [defaults objectForKey:@"color"];
    
    for (int i = 1; i <= 21; i ++) {
        NSString *strName = [NSString stringWithFormat:@"color-%02d", i];
        if ([strColorPics rangeOfString:strName].location == NSNotFound) {
            
            strColorPics = [strColorPics stringByAppendingFormat:@"_%@", strName];
        }
    }
    
//    NSArray *arrColorPic = [strColorPics componentsSeparatedByString:@"_"];
//    for (int i = [arrColorPics count] - 1; i < 21; i ++) {
//        strColorPics = [strColorPics stringByAppendingFormat:@"_%@", @"lock-cover"];
//    }

    arrPics = [strColorPics componentsSeparatedByString:@"_"];
    //arrPics = [NSArray arrayWithObjects:@"color-01.png", @"color-02.png", @"color-03.png", @"color-04.png", @"color-05.png", @"color-06.png", @"color-07.png", @"color-08.png", @"color-09.png", @"color-10.png", @"color-11.png", @"color-12.png", @"color-13.png", @"color-14.png", @"color-15.png", @"color-16.png", @"color-17.png", nil];
    
    [viewiCarousel reloadData];
    [viewForTable setHidden:YES];
}

- (IBAction)purchaseBtnPressed:(id)sender {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSArray *arr = [NSArray array];
    
    NSString *str1 = [defaults objectForKey:@"color"];
    
    NSArray *arr1 = [str1 componentsSeparatedByString:@"_"];
    NSString *str2 = [defaults objectForKey:@"black"];
    NSArray *arr2 = [str2 componentsSeparatedByString:@"_"];
    
    if ([arr1 count]) {
        arr = arr1;
    }
    if ([arr2 count]) {
        arr = [arr arrayByAddingObjectsFromArray:arr2];
    }
    
    [arrCollection removeObjectsInArray:arr];
    if ([arrCollection count] == 0) {
        [lblPurchasedAllItems setHidden:NO];
    }
    else {
        [lblPurchasedAllItems setHidden:YES];
    }
    [myCollectionView reloadData];
    [viewForCollection setHidden:NO];
    viewForCollection.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3/1.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(bounce1AnimationStopped)];
    viewForCollection.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.1, 1.1);
    [UIView commitAnimations];

    
}

- (IBAction)termsNConditionsBtnPressed:(id)sender {
    
    NSString *strTutorial = @"This App (“the App”) is published by American Treasures, LLC, a limited liability company organized under the laws of the Commonwealth of Virginia, USA.  By downloading or otherwise accessing the App, you agree to be bound by the following terms and conditions of use.  If you do not agree to be bound by these terms and conditions, then you should promptly discontinue using the App. \
    \n\n\t1.	You are authorized to use the App for personal use only. You are not permitted to use the App or any photograph or other content featured in the App for any commercial or unlawful purpose.  In your use of the App, you agree to comply with all applicable laws, including but not limited to intellectual property laws, rights of publicity laws, and  privacy laws.\
    \n\t2.	You agree not to use the App in any manner that would encourage or result in illegal activity, including but not limited to copyright infringement and violation of the rights of publicity.\
    \n\t3.	You agree not to adapt, modify, translate or otherwise create any derivative work based on the App, except as authorized for normal and intended use of the App.\
    \n\t4.	The copyright in all material displayed on or accessible through the App, including all information, text, photographs, graphics, software (“the Materials”), and the selection and arrangement of the Materials, is owned by or licensed to American Treasures, LLC. No authorization is given to reproduce, display, transmit, publish, distribute or commercially exploit the Materials other than as authorized for the User to carry out the intended personal use of the App.\
    \n\t5.	FAMOUS PHOTO is a trademark owned or licensed to American Treasures, LLC.  No authorization is given to the User to make any commercial use or to disseminate the FAMOUS PHOTO mark in any manner without the written permission of American Treasures LLC.\
    \n\t6.	You agree to indemnify American Treasures, LLC  against any losses, damages, costs or expenses, including the payment of reasonable attorneys fees in defense thereof, which it may suffer or incur directly or indirectly as a result of your use of the App other than in accordance with these Terms or applicable laws.\
    \n\t7.	This App is provided on an “As Is” basis, to the maximum extent provided by law. American Treasures. LLC makes no warranties, express or implied, and  shall have no liability in connection with the use of the App.  In particular American Treasures, LLC excludes all implied warranties of merchantability and fitness for a particular purpose.  American Treasures, LLC shall have no liability for any technical failure of the internet or the App or any damage to Users’ equipment in connection with their use of the App.\
    \n\t8.	 If American Treasures, LLC should be found liable to you by a court  of law, or other entity having legal authority, in connection with your purchase, installation or use the of the App , then American Treasures’ liability shall be limited to the price paid for the App or the replacement cost of the App, at the election of the User.\
    \n\t9.	These Terms and Conditions may be amended and updated from time to time. You are responsible for regularly reviewing these Terms so that you are aware of any changes to them and you will be bound by the new policy upon your continued use of the App.  Changes to the FAMOUS PHOTO Terms and Condition may be reviewed on the website www.famousphoto.net.\
    \n\t10.	 These Terms and Conditions shall be governed under the laws of the Commonwealth of Virginia.";
    
    TWSReleaseNotesView *releaseNotesView = [TWSReleaseNotesView viewWithReleaseNotesTitle:[NSString stringWithFormat:@"Terms & Conditions"] text:strTutorial closeButtonTitle:@"Close"];
    
    [releaseNotesView showInView:self.view];

}

- (IBAction)tutorialBtnPressed:(id)sender {
//    1.) Select the scene you want to be a part of. \n2.) A big yellow check will then appear. Click Select tab to confirm your selection. \n3.) Your selected scene will appear with a cutout area in which to position your subject in. \n4.) Click Get photo tab then either gallery or camera. Take photo and confirm with Use tab. \nNow you’re ready to add a person to the scene!
    NSString *strTutorial = @"1.) Tap the picture you want to be part of. \
    \n2.) A big yellow check will appear. Tap the \"Select\" tab to confirm your selection. \
    \n3.) Your selected scene will appear with a cut out to position your subject in. \
    \n4.) Tap the \"Add Picture\" tab then either the \"Select from Gallery\" or \"Take Picture\" tab. \
    \n\nOnce you select or take a picture you're ready to adjust the photo into the scene. Your photo will appear as a overlay on the selected scene. Using 2 fingers rotate and size your photo as needed to fit into the cutout area. Tap the \"Preview\" tab to view your completed picture. If you don't like the skin tone or fit simply tap the \"Back\" tab and retake your photo using a different angle or lighting conditions until you're happy. When your finished with your picture tap the \"Done\" tab. Now your ready to share your photo with family, friends and the world via email, post it to Facebook and Twitter, or just save it!\
    \n\nCamera hints \nUse template (cutout area) to get the right angle and pose your subject in. \nLeave a larger border around your subject then adjust as needed. \nDecide if scene was made from upward, downward or level angle and take the same angle. \nIf you need a larger body or longer neck to fill template area take at an upward angle. \nIf you need a larger head and less body area take photo in a downward angle of your subject. Or rotate body as needed to fit in body cutout area. \nOnce you take the photo with your device the photo will appear as an overlay on the selected scene. Use 2 fingers to adjust the size or rotate as needed to fit properly in the cutout area. \nOnce you have positioned in cutout & have the right skin tone click Preview tab. \nIf you don’t get the right skin tone or person to fit, simply click retake tab and use another lighting condition or angle until you’re happy. \n\nGeneral guidelines for all scenes \nDaylight and house lights create different colors of your skin tones so use the different lighting conditions to take great photos. Use this info to adjust your subject's skin tone and color to best match the photo scene. \nColor scenes \nDaylight makes skin normal tone \nHouse light bulbs tones skin more yellow. \nFlash may tone skin red \nBlack & white scenes \nLight background around your subject makes skin darker. \nDark around the subject makes skin lighter. \nLow light and no flash adds grain to your skin to better match older grainy scenes.";
    
    TWSReleaseNotesView *releaseNotesView = [TWSReleaseNotesView viewWithReleaseNotesTitle:[NSString stringWithFormat:@"Famous Photo Tutorial"] text:strTutorial closeButtonTitle:@"Close"];
    
    [releaseNotesView showInView:self.view];
}

- (IBAction)cancelBtnPressed:(id)sender {
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3/1.5];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(hideView)];
    viewForCollection.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.001, 0.001);
    [UIView commitAnimations];
}

- (IBAction)okBtnPressed:(id)sender {
    
    
    for (int i = 0; i < [_products count]; i ++) {
        
        SKProduct *product = _products[i];
        if (![[RageIAPHelper sharedInstance] productPurchased:product.productIdentifier]) {
            [activityIndicator startAnimating];
            [[RageIAPHelper sharedInstance] buyProduct:_products[i]];
            break;
        }
    }
    
    //timer = [NSTimer scheduledTimerWithTimeInterval:2.0 target:self selector:@selector(removeArrCompImageAtIndexes) userInfo:nil repeats:NO];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

#pragma mark -
#pragma mark iCarousel methods

- (NSUInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    return [arrPics count];
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSUInteger)index reusingView:(UIView *)view
{
    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"check-sign"]];
    [imgView setHidden:YES];
    [imgView setContentMode:UIViewContentModeScaleAspectFit];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 200.0f, 200.0f)];
        [imgView setFrame:CGRectMake(0, 0, 200.0f, 200.0f)];
    }
    else {
        view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 500.0f, 500.0f)];
        [imgView setFrame:CGRectMake(0, 0, 500.0f, 500.0f)];
    }
    [view setBackgroundColor:[UIColor clearColor]];
    [(UIImageView*)view setImage:[UIImage imageNamed:arrPics[index]]];
    
    if (![arrColorPics containsObject:arrPics[index]] || ![arrGreyPics containsObject:arrPics[index]]) {
        [imgView setImage:[UIImage imageNamed:@"lock-cover"]];
        [imgView setHidden:NO];
    }
    [(UIImageView*)view addSubview:imgView];
    
    [(UIImageView*)view setContentMode:UIViewContentModeScaleAspectFit];
    
    return view;
}

- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    switch (option)
    {
        case iCarouselOptionWrap:
        {
            return YES;
        }
        default:
        {
            return value;
        }
    }
}

- (void)carouselWillBeginDragging:(iCarousel *)carousel
{
	//NSLog(@"Carousel will begin dragging");
}

- (void)carouselDidEndDragging:(iCarousel *)carousel willDecelerate:(BOOL)decelerate
{
	//NSLog(@"Carousel did end dragging and %@ decelerate", decelerate? @"will": @"won't");
}

- (void)carouselWillBeginDecelerating:(iCarousel *)carousel
{
	//NSLog(@"Carousel will begin decelerating");
}

- (void)carouselDidEndDecelerating:(iCarousel *)carousel
{
	//NSLog(@"Carousel did end decelerating");
}

- (void)carouselWillBeginScrollingAnimation:(iCarousel *)carousel
{
	//NSLog(@"Carousel will begin scrolling");
}

- (void)carouselDidEndScrollingAnimation:(iCarousel *)carousel
{
    //NSLog(@"%d", carousel.currentItemIndex);
    
    currentImageIndex = carousel.currentItemIndex;
    
    for (int i=0; i<viewiCarousel.numberOfItems; i++) {
        UIImageView *imgv = (UIImageView *)[viewiCarousel itemViewAtIndex:i];
//        if ((UIImageView *)carousel.currentItemView != imgv) {
//            if (i != carousel.currentItemIndex && ![arrPics[i] isEqualToString:@"lock-cover"]) {
            if ([arrColorPics containsObject:arrPics[i]] || [arrGreyPics containsObject:arrPics[i]]) {
                UIImageView *img = imgv.subviews[0];
                [img setHidden:YES];
            }
            //[imgv setHighlighted:NO];
//        }
    }
}

- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index {
    //NSLog(@"selected index %d", index);

    UIImageView *imgv = (UIImageView *)[viewiCarousel itemViewAtIndex:index];
    if ((UIImageView *)carousel.currentItemView == imgv) {
    
        //if (![arrPics[index] isEqualToString:@"lock-cover"]) {
        if ([arrColorPics containsObject:arrPics[index]] || [arrGreyPics containsObject:arrPics[index]]) {
            
            UIImageView *img = imgv.subviews[0];
            [img setImage:[UIImage imageNamed:@"check-sign"]];
            [img setHidden:NO];
        }
        //[(UIImageView *)carousel.currentItemView setHighlighted:YES];
    }
}

#pragma mark --
#pragma mark -- InAppPurchases

- (void)productPurchased:(NSNotification *)notification {
    
    NSString * productIdentifier = notification.object;
    [_products enumerateObjectsUsingBlock:^(SKProduct * product, NSUInteger idx, BOOL *stop) {
        if ([product.productIdentifier isEqualToString:productIdentifier]) {
            NSLog(@"product %@, index %d", product.productIdentifier, idx);
            [self removeArrCompImageAtIndexes];
            [activityIndicator stopAnimating];
            
//            switch (idx) {
//                case 0:
//                    [btnDoublePhoto setHidden:YES];
//                    [self photoBtnPressed:btnDoublePhoto];
//                    break;
//                case 1:
//                    [btnTriplePhoto setHidden:YES];
//                    [self photoBtnPressed:btnTriplePhoto];
//                    break;
//                default:
//                    break;
//            }
            
            *stop = YES;
        }
    }];
    
}

- (void)reload {
    _products = nil;
    [[RageIAPHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
        
        [self.view setUserInteractionEnabled:YES];
        //[indicatorView stopAnimating];
        
        if (success) {
            _products = products;
            NSLog(@"products %@", _products);
            
//            if ([_products count]) {
//                SKProduct *product1 = _products[0];
//                if ([[RageIAPHelper sharedInstance] productPurchased:product1.productIdentifier]) {
//                    
//                    //UILabel *lbl = (UILabel *)[self.view viewWithTag:123];
//                    //[lbl setCenter:CGPointMake(200, 180)];
//                    [btnDoublePhoto setHidden:YES];
//                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"product1"];
//                }
//                SKProduct *product2 = _products[1];
//                if ([[RageIAPHelper sharedInstance] productPurchased:product2.productIdentifier]) {
//                    
//                    //UILabel *lbl = (UILabel *)[self.view viewWithTag:124];
//                    //CGRect rect = lbl.frame;
//                    //[lbl setCenter:CGPointMake(200, 330)];
//                    
//                    [btnTriplePhoto setHidden:YES];
//                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"product2"];
//                }
//            }
            
            //[self.tableView reloadData];
        }
        //[self.refreshControl endRefreshing];
    }];
}


#pragma mark --
#pragma mark -- UICollectionView Delegates

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [arrCollection count];
}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *myCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CollectionViewCell" forIndexPath:indexPath];
    
    NSString *string = [arrCollection objectAtIndex:indexPath.row];
    UIImageView *imageView = (UIImageView *)[myCell viewWithTag:1000];
    [imageView setImage:[UIImage imageNamed:string]];
    UIButton *button = (UIButton *)[myCell viewWithTag:1001];
    
    if ([arrPurchasedImages containsObject:arrCollection[indexPath.row]]) {
        [button setSelected:YES];
    }
    else {
        [button setSelected:NO];
    }
    
    return myCell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    UIButton *button = (UIButton *)[cell viewWithTag:1001];
    if ([button isSelected]) {
        UIImageView *imageView = (UIImageView *)[cell viewWithTag:1000];

        [UIView transitionWithView:imageView duration:1.0 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
            [self startJiggling:imageView];
        } completion:^(BOOL finished) {
            [self stopJiggling:imageView];
            [imageView setAlpha:1.0];
            [button setSelected:NO];
        }];

        [button setSelected:NO];
        NSString *str1 = [arrCollection objectAtIndex:indexPath.row];
        [arrPurchasedImages removeObject:str1];
        count--;
        [okBtn setHidden:YES];
    }
    else {
        if (count == 7){
            UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Sorry" message:@"Enough images are selected" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alertView show];
        }
        else {
            count++;
            UIImageView *imageView = (UIImageView *)[cell viewWithTag:1000];
            
            [ UIView transitionWithView:imageView duration:1.0 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
                [imageView setAlpha:0.2];
                [button setSelected:YES];
                NSString *str = [arrCollection objectAtIndex:indexPath.row];
                [arrPurchasedImages addObject:str];
                
            } completion:^(BOOL finished) {
                if (count == 7){
                    [okBtn setHidden:NO];
                }
                [imageView setAlpha:1.0];
            }];
        }
    }
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)bounce1AnimationStopped {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3/2];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(bounce2AnimationStopped)];
    viewForCollection.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.9, 0.9);
    [UIView commitAnimations];
}

- (void)bounce2AnimationStopped {
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.3/2];
    viewForCollection.transform = CGAffineTransformIdentity;
    [UIView commitAnimations];
}

- (void)startJiggling:(UIImageView *)imageView {
    
    NSInteger randomInt = arc4random_uniform(500);
    float r = (randomInt/500.0)+1.5;
    
    CGAffineTransform leftWobble = CGAffineTransformMakeRotation(degreesToRadians( (kAnimationRotateDeg * -1.0) - r ));
    CGAffineTransform rightWobble = CGAffineTransformMakeRotation(degreesToRadians( kAnimationRotateDeg + r ));
    
    imageView.transform = leftWobble;  // starting point
    
    [[imageView layer] setAnchorPoint:CGPointMake(0.5, 0.5)];
    
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionAllowUserInteraction | UIViewAnimationOptionRepeat | UIViewAnimationOptionAutoreverse
                     animations:^{
                         [UIView setAnimationRepeatCount:NSNotFound];
                         imageView.transform = rightWobble; }
                     completion:^(BOOL finished) {
                     }];
}

- (void)stopJiggling:(UIImageView *)imageView {
    [imageView.layer removeAllAnimations];
    imageView.transform = CGAffineTransformIdentity;
}

- (void)removeArrCompImageAtIndexes{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF BEGINSWITH[c] %@", @"grey"];
    NSArray *arr1 = [arrPurchasedImages filteredArrayUsingPredicate:predicate];
    
    [arrPurchasedImages removeObjectsInArray:arr1];
    
    if ([arrPurchasedImages count]) {
        
        NSString *strJoint = [arrPurchasedImages componentsJoinedByString:@"_"];
        if ([[defaults objectForKey:@"color"] length] > 0) {
            
            strJoint = [strJoint stringByAppendingFormat:@"_%@", [defaults objectForKey:@"color"]];
        }
        [defaults setObject:strJoint forKey:@"color"];
    }
    if ([arr1 count]) {
        NSString *strJoint = [arr1 componentsJoinedByString:@"_"];
        if ([[defaults objectForKey:@"black"] length] > 1) {
            
            strJoint = [strJoint stringByAppendingFormat:@"_%@", [defaults objectForKey:@"black"]];
        }
        [defaults setObject:strJoint forKey:@"black"];
    }
    [defaults synchronize];

    
    [arrCollection removeObjectsInArray:arrPurchasedImages];
    [arrCollection removeObjectsInArray:arr1];
    [arrPurchasedImages removeAllObjects];
    [activityIndicator stopAnimating];
    [okBtn setHidden:YES];
    [UIView transitionWithView:myCollectionView duration:0.8 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        [myCollectionView reloadData];
        
    } completion:^(BOOL finished){
        [UIView transitionWithView:viewForCollection duration:0.8 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
            [viewForCollection setHidden:YES];
        } completion:^(BOOL finished) {
            
            arrGreyPics = [[defaults objectForKey:@"black"] componentsSeparatedByString:@"_"];
            arrColorPics = [[defaults objectForKey:@"color"] componentsSeparatedByString:@"_"];
            if (categoryIndex == 0) {
                //arrPics = [[defaults objectForKey:@"black"] componentsSeparatedByString:@"_"];
                NSString *strGreyPics = [defaults objectForKey:@"black"];
                
                for (int i = 1; i <= 19; i ++) {
                    NSString *strName = [NSString stringWithFormat:@"grey-%02d", i];
                    if ([strGreyPics rangeOfString:strName].location == NSNotFound) {
                        
                        strGreyPics = [strGreyPics stringByAppendingFormat:@"_%@", strName];
                    }
                }
                
//                NSArray *arrGreyPic = [strGreyPics componentsSeparatedByString:@"_"];
//                for (int i = [arrGreyPic count] - 1; i < 19; i ++) {
//                    strGreyPics = [strGreyPics stringByAppendingFormat:@"_%@", @"lock-cover"];
//                }
                arrPics = [strGreyPics componentsSeparatedByString:@"_"];
            }
            else {
                NSString *strColorPics = [defaults objectForKey:@"color"];
                
                for (int i = 1; i <= 21; i ++) {
                    NSString *strName = [NSString stringWithFormat:@"color-%02d", i];
                    if ([strColorPics rangeOfString:strName].location == NSNotFound) {
                        
                        strColorPics = [strColorPics stringByAppendingFormat:@"_%@", strName];
                    }
                }
                
//                NSArray *arrColorPic = [strColorPics componentsSeparatedByString:@"_"];
//                for (int i = [arrColorPic count] - 1; i < 21; i ++) {
//                    strColorPics = [strColorPics stringByAppendingFormat:@"_%@", @"lock-cover"];
//                }

                arrPics = [strColorPics componentsSeparatedByString:@"_"];
            }
            [viewiCarousel reloadData];
        }];
    }];
    count = 0;
}

- (void)hideView {
    [viewForCollection setHidden:YES];
    [viewForCollection setTransform:CGAffineTransformIdentity];
}


@end
