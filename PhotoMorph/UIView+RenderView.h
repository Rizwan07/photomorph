//
//  UIView+RenderView.h
//  PhotoMorph
//
//  Created by Rizwan on 9/26/13.
//  Copyright (c) 2013 com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (RenderView)

- (UIImage *)takeSnapshot;

@end
