//
//  DetailViewController.h
//  PhotoMorph
//
//  Created by Rizwan on 5/15/13.
//  Copyright (c) 2013 com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController<UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPopoverControllerDelegate, UIGestureRecognizerDelegate> {
    
    UIPopoverController *popOver;

    IBOutlet UIView *viewPopup;
    IBOutlet UIImageView *imageViewback;
    //IBOutlet UIView *viewRender;
    IBOutlet UIView *viewColor;
    IBOutlet UISlider *sliderHue;
    IBOutlet UISlider *sliderSat;
    IBOutlet UISlider *sliderBri;
}

@property (nonatomic, strong) IBOutlet UIView *viewRender;

@property (nonatomic, strong) NSString *strUpperImage;
@property (nonatomic, assign) int categoryIndex;

- (IBAction)addPictureBtnPressed:(id)sender;
- (IBAction)backBtnPressed:(id)sender;

- (IBAction)takePictureBtnPressed:(id)sender;
- (IBAction)selectFromGalleryBtnPressed:(id)sender;

- (IBAction)previewBtnPressed:(id)sender;
- (IBAction)tutorialBtnPressed:(id)sender;

- (IBAction)colorSetBtnPressed:(id)sender;

- (IBAction)hueBtnPressed:(id)sender;
- (IBAction)satBtnPressed:(id)sender;
- (IBAction)briBtnPressed:(id)sender;

@end
