//
//  UIImagePickerController+NonRotating.m
//  PhotoMorph
//
//  Created by Rizwan on 5/15/13.
//  Copyright (c) 2013 com. All rights reserved.
//

#import "UIImagePickerController+NonRotating.h"

@implementation UIImagePickerController (NonRotating)

- (BOOL)shouldAutorotate
{
    return NO;
}
//
//-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
//{
//    return UIInterfaceOrientation;
//}

@end
