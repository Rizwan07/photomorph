//
//  SharingViewController.m
//  PhotoMorph
//
//  Created by Rizwan on 5/17/13.
//  Copyright (c) 2013 com. All rights reserved.
//

#import "SharingViewController.h"
#import "TWSReleaseNotesView.h"

@interface SharingViewController ()

@end

@implementation SharingViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [_imageView setImage:_image];
    
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (IBAction)backBtnPressed:(id)sender {
    
    UIViewController *viewController = [[self.navigationController viewControllers] objectAtIndex:1];
    [self.navigationController popToViewController:viewController animated:YES];
}

- (IBAction)homeBtnPressed:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)shareBtnsPressed:(id)sender {
    
    int tag = [sender tag];
    switch (tag) {

        case 0:
            if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook]) {
                SLComposeViewController *fbSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
                [fbSheet setInitialText:@"Enter Message :"];
                
                [fbSheet addImage:_imageView.image];
                [fbSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
                    
                    switch (result) {
                        case SLComposeViewControllerResultCancelled:
                            NSLog(@"Post Canceled");
                            break;
                        case SLComposeViewControllerResultDone:
                            //[self titleOKBtnPressed:nil];
                            break;
                            
                        default:
                            break;
                    }
                }];
                [self presentViewController:fbSheet animated:YES completion:nil];
                
            }
            else {
                UIAlertView *alrtView = [[UIAlertView alloc] initWithTitle:nil message:@"You can't send a message right now, make sure your device has an internet connection and have at least one facebook account setup. Sorry" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alrtView show];
            }
            
            break;
        case 1:
            if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter]) {
                SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
                [tweetSheet setInitialText:@"Enter Message :"];
                
                [tweetSheet addImage:_imageView.image];
                [tweetSheet setCompletionHandler:^(SLComposeViewControllerResult result) {
                    
                    switch (result) {
                        case SLComposeViewControllerResultCancelled:
                            NSLog(@"Post Canceled");
                            break;
                        case SLComposeViewControllerResultDone:
                            break;
                            
                        default:
                            break;
                    }
                }];
                
                [self presentViewController:tweetSheet animated:YES completion:nil];
            }
            else {
                UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:nil message:@"You can't send a tweet right now, make sure your device has an internet connection and have at least one twitter account setup. Sorry" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                
                [alertView show];
            }
            
            break;
        case 2:
            if ([MFMailComposeViewController canSendMail]) {
                // Initialization
                MFMailComposeViewController *vc = [[MFMailComposeViewController alloc] init];
                
                [vc setSubject:@"Famous Photo"];
                NSData *imageData = UIImagePNGRepresentation(_imageView.image);
                [vc addAttachmentData:imageData mimeType:@"image/png" fileName:@"memeo.png"];
                
                [vc setMailComposeDelegate:self];
                
                [self presentViewController:vc animated:YES completion:nil];
            }
            break;
        case 3:
            UIImageWriteToSavedPhotosAlbum(_imageView.image, self, @selector(image:didFinishSavingWithError:contextInfo:), nil);
            
        default:
            break;
    }
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo: (void *) contextInfo {
    
    if (error != nil) {
        NSLog(@"Error %@", error.localizedDescription);
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Saved to photo album" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    
    [controller dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)tutorialBtnPressed:(id)sender {
    
//    RNBlurModalView *modal = [[RNBlurModalView alloc] initWithViewController:self title:@"Hello world!" message:@"This is test tutorial messages, and text will be updated soooooooooooooooooooooooonnnnnnnnn\n This is test tutorial messages, and text will be updated soooooooooooooooooooooooonnnnnnnnn. This is test tutorial messages, and text will be updated soooooooooooooooooooooooonnnnnnnnn"];
//    [modal show];
    
    NSString *strTutorial = @"1.) Tap the picture you want to be part of. \
    \n2.) A big yellow check will appear. Tap the \"Select\" tab to confirm your selection. \
    \n3.) Your selected scene will appear with a cut out to position your subject in. \
    \n4.) Tap the \"Add Picture\" tab then either the \"Select from Gallery\" or \"Take Picture\" tab. \
    \n\nOnce you select or take a picture you're ready to adjust the photo into the scene. Your photo will appear as a overlay on the selected scene. Using 2 fingers rotate and size your photo as needed to fit into the cutout area. Tap the \"Preview\" tab to view your completed picture. If you don't like the skin tone or fit simply tap the \"Back\" tab and retake your photo using a different angle or lighting conditions until you're happy. When your finished with your picture tap the \"Done\" tab. Now your ready to share your photo with family, friends and the world via email, post it to Facebook and Twitter, or just save it!\
    \n\nCamera hints \nUse template (cutout area) to get the right angle and pose your subject in. \nLeave a larger border around your subject then adjust as needed. \nDecide if scene was made from upward, downward or level angle and take the same angle. \nIf you need a larger body or longer neck to fill template area take at an upward angle. \nIf you need a larger head and less body area take photo in a downward angle of your subject. Or rotate body as needed to fit in body cutout area. \nOnce you take the photo with your device the photo will appear as an overlay on the selected scene. Use 2 fingers to adjust the size or rotate as needed to fit properly in the cutout area. \nOnce you have positioned in cutout & have the right skin tone click Preview tab. \nIf you don’t get the right skin tone or person to fit, simply click retake tab and use another lighting condition or angle until you’re happy. \n\nGeneral guidelines for all scenes \nDaylight and house lights create different colors of your skin tones so use the different lighting conditions to take great photos. Use this info to adjust your subject's skin tone and color to best match the photo scene. \nColor scenes \nDaylight makes skin normal tone \nHouse light bulbs tones skin more yellow. \nFlash may tone skin red \nBlack & white scenes \nLight background around your subject makes skin darker. \nDark around the subject makes skin lighter. \nLow light and no flash adds grain to your skin to better match older grainy scenes.";
    
    TWSReleaseNotesView *releaseNotesView = [TWSReleaseNotesView viewWithReleaseNotesTitle:[NSString stringWithFormat:@"Famous Photo Tutorial"] text:strTutorial closeButtonTitle:@"Close"];
    
    // Show the release notes view
    [releaseNotesView showInView:self.view];

}

@end
