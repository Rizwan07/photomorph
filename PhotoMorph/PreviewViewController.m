//
//  PreviewViewController.m
//  PhotoMorph
//
//  Created by Rizwan on 5/17/13.
//  Copyright (c) 2013 com. All rights reserved.
//

#import "PreviewViewController.h"
#import "SharingViewController.h"

@interface PreviewViewController ()

- (IBAction)backBtnPressed:(id)sender;
@end

@implementation PreviewViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [_imageViewPreview setImage:_image];
    
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (IBAction)doneBtnPressed:(id)sender {
    
    SharingViewController *sharingViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SharingViewController"];
    [sharingViewController setImage:_imageViewPreview.image];
    [self.navigationController pushViewController:sharingViewController animated:YES];
    //[sharingViewController.imageView setImage:_imageViewPreview.image];
    
    //[self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)backBtnPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
