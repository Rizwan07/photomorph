//
//  DetailViewController.m
//  PhotoMorph
//
//  Created by Rizwan on 5/15/13.
//  Copyright (c) 2013 com. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "AppDelegate.h"

#import "DetailViewController.h"
#import "PreviewViewController.h"
#import "UIImagePickerController+NonRotating.h"

#import "UIView+RenderView.h"

@interface DetailViewController () {
    

    UITapGestureRecognizer *_tapGesture;
    UIImagePickerController *_pickerController;
    
    UIImageView *imageViewFront;
    
    UIPinchGestureRecognizer *pinchGesture;
    UIRotationGestureRecognizer *rotateGesture;
    
    CGFloat hue;
    CGFloat saturation;
    CGFloat brightness;
    //CGFloat alpha;
}
@property (nonatomic, retain)UIImagePickerController *imagePickerController;

- (void)handleTap:(UITapGestureRecognizer *)recognizer;

@end

@implementation DetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [imageViewback setImage:[UIImage imageNamed:_strUpperImage]];
    
    CGSize size = [[UIScreen mainScreen] bounds].size;
    
    imageViewFront = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, size.height, size.width)];
    [imageViewFront setContentMode:UIViewContentModeScaleAspectFill];
    [imageViewFront setUserInteractionEnabled:YES];
    [imageViewFront setAlpha:0.7];
    [imageViewFront setHidden:YES];
    [_viewRender addSubview:imageViewFront];

    UIPanGestureRecognizer *panGesture = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePan:)];
    [panGesture setDelegate:self];
    [panGesture setMinimumNumberOfTouches:1];
    [imageViewFront addGestureRecognizer:panGesture];

    pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinch:)];
    [pinchGesture setDelegate:self];
    [pinchGesture setScale:1.0];
    [imageViewFront addGestureRecognizer:pinchGesture];

    rotateGesture = [[UIRotationGestureRecognizer alloc] initWithTarget:self action:@selector(handleRotate:)];
    [rotateGesture setDelegate:self];
    [imageViewFront addGestureRecognizer:rotateGesture];

    
    _tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapView)];
    [_tapGesture setNumberOfTapsRequired:1];
    [_tapGesture setNumberOfTouchesRequired:1];
    
    self.imagePickerController = [[UIImagePickerController alloc]init];
    _imagePickerController.delegate = self;
    _imagePickerController.allowsEditing = NO;
    _imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) {
        
        popOver = [[UIPopoverController alloc]initWithContentViewController:_imagePickerController];
        [popOver setDelegate:self];
    }
}

- (UIImage*) imageWithImage:(UIImage*) source fixedHue:(CGFloat) hu saturation:(CGFloat)sat bright:(CGFloat)bri alpha:(CGFloat) alpha;
// Note: the hue input ranges from 0.0 to 1.0, both red.  Values outside this range will be clamped to 0.0 or 1.0.
{
    // Find the image dimensions.
    CGSize imageSize = [source size];
    CGRect imageExtent = CGRectMake(0,0,imageSize.width,imageSize.height);
    
    // Create a context containing the image.
    UIGraphicsBeginImageContext(imageSize);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [source drawAtPoint:CGPointMake(0,0)];
    
    // Draw the hue on top of the image.
    CGContextSetBlendMode(context, kCGBlendModeHue);
    [[UIColor colorWithHue:hu saturation:sat brightness:bri alpha:alpha] set];
    UIBezierPath *imagePath = [UIBezierPath bezierPathWithRect:imageExtent];
    [imagePath fill];
    
    // Retrieve the new image.
    UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return result;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}


-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [_viewRender bringSubviewToFront:imageViewFront];
    [imageViewFront setAlpha:0.7];
}

- (IBAction)handlePan:(UIPanGestureRecognizer *)recognizer {
    
    CGPoint translation = [recognizer translationInView:self.view];
    recognizer.view.center = CGPointMake(recognizer.view.center.x + translation.x,
                                         recognizer.view.center.y + translation.y);
    [recognizer setTranslation:CGPointMake(0, 0) inView:self.view];
    
}

- (IBAction)handlePinch:(UIPinchGestureRecognizer *)recognizer {
    
    recognizer.view.transform = CGAffineTransformScale(recognizer.view.transform, recognizer.scale, recognizer.scale);
    recognizer.scale = 1;
    
}

- (IBAction)handleRotate:(UIRotationGestureRecognizer *)recognizer {
    
    recognizer.view.transform = CGAffineTransformRotate(recognizer.view.transform, recognizer.rotation);
    recognizer.rotation = 0;
    
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

- (void)handleTap:(UITapGestureRecognizer *)recognizer {
    //[self.chompPlayer play];
}

- (void)tapView {
    
    [self.view setUserInteractionEnabled:NO];
    
    [UIView transitionWithView:viewPopup duration:0.5 options:UIViewAnimationOptionTransitionCurlUp animations:^{
        [viewPopup setHidden:YES];
    } completion:^(BOOL finished) {
        
        [self.view removeGestureRecognizer:_tapGesture];
        [self.view setUserInteractionEnabled:YES];
    }];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSUInteger)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskLandscape;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (IBAction)addPictureBtnPressed:(id)sender {
    
    [self.view bringSubviewToFront:viewPopup];
    
    if ([viewPopup isHidden]) {
        
        [self.view setUserInteractionEnabled:NO];
        
        [UIView transitionWithView:viewPopup duration:0.5 options:UIViewAnimationOptionTransitionCurlDown animations:^{
            [viewPopup setHidden:NO];
        } completion:^(BOOL finished) {
            
            [self.view addGestureRecognizer:_tapGesture];
            [self.view setUserInteractionEnabled:YES];
        }];
    }
    else {
        [self tapView];
    }
}

- (IBAction)backBtnPressed:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)takePictureBtnPressed:(id)sender {
    

    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:_strUpperImage]];
    [imgView setContentMode:UIViewContentModeScaleToFill];
    [imgView setAlpha:0.8];
    [imgView setUserInteractionEnabled:YES];
    [imgView.layer setOpaque:NO];
    [imgView setOpaque:NO];
    CGSize size = [[UIScreen mainScreen] bounds].size;
    int height = ([[UIDevice currentDevice] userInterfaceIdiom]== UIUserInterfaceIdiomPhone) ? 320 : 768;
    
    [imgView setFrame:CGRectMake(0, 0, size.height, height)];
    
    UIView *overlayView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, size.height, height)];
    
    [overlayView addSubview:imgView];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom]== UIUserInterfaceIdiomPhone){
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        appDelegate.isPickerController = YES;
        
        if (size.height == 568) {
            [imgView setFrame:CGRectMake(0, 0, size.height-96, height)];
        }
        else {
            [imgView setFrame:CGRectMake(0, 0, size.height-44, height)];
        }
        
        UIButton *btnBack = [[UIButton alloc] initWithFrame:CGRectMake(270, 20, 54, 34)];
        [btnBack setBackgroundImage:[UIImage imageNamed:@"back-button.png"] forState:UIControlStateNormal];
        [btnBack setAlpha:0.6];
        [btnBack setShowsTouchWhenHighlighted:YES];
        [btnBack addTarget:self action:@selector(dismissPicker) forControlEvents:UIControlEventTouchUpInside];
        [btnBack setUserInteractionEnabled:YES];
        [btnBack setTransform:CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(90))];
        [overlayView addSubview:btnBack];
        
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(20, 20, 60, 80)];
//        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(size.width/2-40, size.height-60, 80, 60)];
        [btn setAlpha:0.8];
        [btn setBackgroundImage:[UIImage imageNamed:@"camera-icon-yellow.png"] forState:UIControlStateNormal];
        [btn setShowsTouchWhenHighlighted:YES];
        [btn addTarget:self action:@selector(capture) forControlEvents:UIControlEventTouchUpInside];
        //[btn setUserInteractionEnabled:YES];
        [overlayView addSubview:btn];
    }
    
    if ([[UIDevice currentDevice] userInterfaceIdiom]== UIUserInterfaceIdiomPhone){
        
        if (size.height == 568) {
            [imgView setCenter:CGPointMake(size.width/2, size.height/2-48)];
            CGAffineTransform transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(90));
            [imgView setTransform:transform];
            [imgView setCenter:CGPointMake(size.width/2, size.height/2-48)];
        }
        else {
            [imgView setCenter:CGPointMake(size.width/2, size.height/2-22)];
            CGAffineTransform transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(90));
            [imgView setTransform:transform];
            [imgView setCenter:CGPointMake(size.width/2, size.height/2-22)];
        }
    }
    else {
        UIButton *btnBack = [[UIButton alloc] initWithFrame:CGRectMake(20, 20, 96, 62)];
        [btnBack setBackgroundImage:[UIImage imageNamed:@"back-button.png"] forState:UIControlStateNormal];
        [btnBack setAlpha:0.6];
        [btnBack setShowsTouchWhenHighlighted:YES];
        [btnBack addTarget:self action:@selector(dismissPicker) forControlEvents:UIControlEventTouchUpInside];
        [btnBack setUserInteractionEnabled:YES];
        [overlayView addSubview:btnBack];
        
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(20, 668, 60, 80)];
        [btn setAlpha:0.8];
        [btn setBackgroundImage:[UIImage imageNamed:@"camera-icon-yellow.png"] forState:UIControlStateNormal];
        [btn setShowsTouchWhenHighlighted:YES];
        [btn addTarget:self action:@selector(capture) forControlEvents:UIControlEventTouchUpInside];
        [btn setTransform:CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(-90))];
        [overlayView addSubview:btn];

    }
    _pickerController = [[UIImagePickerController alloc] init];
    //[_pickerController.view setFrame:CGRectMake(0, 0, size.height, height)];
    [_pickerController.view setFrame:CGRectMake(0, 0, height, size.height)];
    [_pickerController setDelegate:self];
    if (!TARGET_IPHONE_SIMULATOR) {
        [_pickerController setSourceType:UIImagePickerControllerSourceTypeCamera];
    }
    [_pickerController setModalPresentationStyle:UIModalPresentationFullScreen];
    [_pickerController setCameraOverlayView:overlayView];
    
//    if ([[UIDevice currentDevice] userInterfaceIdiom]== UIUserInterfaceIdiomPhone){
        [_pickerController setShowsCameraControls:NO];
//    }
    [_pickerController setWantsFullScreenLayout:YES];
    //_pickerController.cameraViewTransform = CGAffineTransformScale(_pickerController.cameraViewTransform, 1.0, 1.12412);
    [self presentViewController:_pickerController animated:YES completion:nil];

}

- (void)capture {
    
    [_pickerController takePicture];
}

- (void)dismissPicker {
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.isPickerController = NO;

    [_pickerController dismissViewControllerAnimated:YES completion:nil];
}

- (UIImage *)convertImageToGrayScale:(UIImage *)image
{
    
    CGSize imageSize = image.size;
    
    // Create image rectangle with current image width/height
    CGRect imageRect = CGRectMake(0, 0, imageSize.width, imageSize.height);
    
    // Grayscale color space
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceGray();
    
    // Create bitmap content with current image size and grayscale colorspace
    CGContextRef context = CGBitmapContextCreate(nil, imageSize.width, imageSize.height, 8, 0, colorSpace, (CGBitmapInfo)kCGImageAlphaNone);
    
    // Draw image into current context, with specified rectangle
    // using previously defined context (with grayscale colorspace)
    CGContextDrawImage(context, imageRect, [image CGImage]);
    
    // Create bitmap image info from pixel data in current context
    CGImageRef imageRef = CGBitmapContextCreateImage(context);
    
    // Create a new UIImage object
    UIImage *newImage = [UIImage imageWithCGImage:imageRef];
    
    // Release colorspace, context and bitmap information
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
    CFRelease(imageRef);
    
    // Return the new grayscale image
    return newImage;
}

- (UIImage *)getCurrentImageContext {
    
    [imageViewFront setAlpha:1.0];
    [_viewRender sendSubviewToBack:imageViewFront];
    
    CGSize size = [[UIScreen mainScreen] bounds].size;
    int height = ([[UIDevice currentDevice] userInterfaceIdiom]== UIUserInterfaceIdiomPhone) ? 320 : 768;
    
    UIImage *newImage;
    
    if ([[[UIDevice currentDevice] systemVersion] doubleValue] >= 7.0) {
        // IOS 7
        
//        CGRect screenRect = CGRectMake(0, 0, size.height, height);//[[UIScreen mainScreen] bounds];
//        UIGraphicsBeginImageContext(screenRect.size);
//        CGContextRef ctx = UIGraphicsGetCurrentContext();
//        //[[UIColor blackColor] set];
//        CGContextFillRect(ctx, screenRect);
//        //[[[[UIApplication sharedApplication] delegate] window].layer renderInContext:ctx];
//        [_viewRender.layer renderInContext:ctx];
//        newImage = UIGraphicsGetImageFromCurrentImageContext();
//        UIGraphicsEndImageContext();
        
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(size.height, height), NO, [UIScreen mainScreen].scale);
        [_viewRender drawViewHierarchyInRect:CGRectMake(0, 0, size.height, height) afterScreenUpdates:YES];
        
        newImage = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
    }
    else {
        // IOS 6
        
        UIGraphicsBeginImageContext( CGSizeMake(size.height, height) );
        [_viewRender.layer renderInContext:UIGraphicsGetCurrentContext()];
        newImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
    }
    return newImage;
}

- (IBAction)selectFromGalleryBtnPressed:(id)sender {
    
    if  ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        appDelegate.isPickerController = YES;

        _pickerController = [[UIImagePickerController alloc] init];
        [_pickerController setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
        [_pickerController setDelegate:self];
        [self presentViewController:_pickerController animated:YES completion:nil];
    }
    else {
        UIButton *button = (UIButton *)sender;
        [popOver presentPopoverFromRect:button.frame inView:button permittedArrowDirections: UIPopoverArrowDirectionAny animated:YES];
    }
}

- (IBAction)previewBtnPressed:(id)sender {
    
    UIImage *image = [self getCurrentImageContext];
    
    NSLog(@"image %@", image);
    
    PreviewViewController *previewViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"PreviewViewController"];
    //[previewViewController setModalTransitionStyle:UIModalTransitionStyleCoverVertical];
    [previewViewController setImage:image];
    [self.navigationController pushViewController:previewViewController animated:YES];
}

- (IBAction)tutorialBtnPressed:(id)sender {
    
    
}

- (IBAction)colorSetBtnPressed:(id)sender {
    
    UIButton *btn = (UIButton *)sender;
    [viewColor setHidden:[btn isSelected]];
    [btn setSelected:![btn isSelected]];
    
}

- (IBAction)hueBtnPressed:(id)sender {
    
    if (hue - ((UISlider *)sender).value > 0.5 || hue - ((UISlider *)sender).value < 0.5) {
        
        hue = ((UISlider *)sender).value;
        
        UIImage *img = [self imageWithImage:imageViewFront.image fixedHue:hue saturation:saturation bright:brightness alpha:0.7];
        [imageViewFront setImage:img];
    }
}

- (IBAction)satBtnPressed:(id)sender {
    
    saturation = ((UISlider *)sender).value;
    
    UIImage *img = [self imageWithImage:imageViewFront.image fixedHue:hue saturation:saturation bright:brightness alpha:0.7];
    [imageViewFront setImage:img];

}

- (IBAction)briBtnPressed:(id)sender {
    
    brightness = ((UISlider *)sender).value;
    
    UIImage *img = [self imageWithImage:imageViewFront.image fixedHue:hue saturation:saturation bright:brightness alpha:0.7];
    [imageViewFront setImage:img];

}

#pragma mark --
#pragma mark -- UIImagePickerController Delegates

//- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage *)image editingInfo:(NSDictionary *)editingInfo {
//    
//    NSLog(@"hello");
//}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.isPickerController = NO;

    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    UIImage *img;
    if (_categoryIndex == 0) {
        img = [self convertImageToGrayScale:image];
    }
    else {
        img = image;//[self imageWithImage:image fixedHue:24.0/360.0 saturation:.75 bright:.80 alpha:1.0];
    }
    [imageViewFront setImage:img];
    if (_imagePickerController == picker) {
        [popOver dismissPopoverAnimated:YES];
    }
    else {
        [picker dismissViewControllerAnimated:YES completion:nil];
    }
    
    [pinchGesture.view setTransform:CGAffineTransformIdentity];
    [rotateGesture.view setTransform:CGAffineTransformIdentity];
    [imageViewFront setCenter:self.view.center];
    [imageViewFront setHidden:NO];
    
    [self tapView];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.isPickerController = NO;

    [picker dismissViewControllerAnimated:YES completion:nil];
}

@end
