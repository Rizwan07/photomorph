//
//  ViewController.h
//  PhotoMorph
//
//  Created by Rizwan on 5/8/13.
//  Copyright (c) 2013 com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"

@interface HomeViewController : UIViewController<UIGestureRecognizerDelegate> {
    
    IBOutlet UIView *viewForTable;
    IBOutlet iCarousel *viewiCarousel;
}

- (IBAction)doneBtnPressed:(id)sender;
- (IBAction)arrowBackBtnPressed:(id)sender;
- (IBAction)arrowForwardBtnPressed:(id)sender;
- (IBAction)categoryBtnPressed:(id)sender;
- (IBAction)purchaseBtnPressed:(id)sender;
- (IBAction)termsNConditionsBtnPressed:(id)sender;
- (IBAction)tutorialBtnPressed:(id)sender;

@end
