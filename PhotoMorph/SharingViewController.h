//
//  SharingViewController.h
//  PhotoMorph
//
//  Created by Rizwan on 5/17/13.
//  Copyright (c) 2013 com. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import <Social/Social.h>

@interface SharingViewController : UIViewController<MFMailComposeViewControllerDelegate>

@property (strong, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) UIImage *image;
- (IBAction)backBtnPressed:(id)sender;
- (IBAction)homeBtnPressed:(id)sender;
- (IBAction)shareBtnsPressed:(id)sender;
- (IBAction)tutorialBtnPressed:(id)sender;
@end
